QUnit.test("calculadora.sumar, con dos numeros enteros, retorna la suma de ambos", function (assert) {
    var resultado = calculadora.sumar(1, 2);
    assert.equal(3, resultado);
});

QUnit.test("calculadora.sumar, con algun parametro mal, lanza error", function (assert) {
    var mensajeEsperado = "El metodo espera 2 numeros enteros";
    assert.throws(function () {
        calculadora.sumar("aa", 2);
    }, mensajeEsperado);
    assert.throws(function () {
        calculadora.sumar(2, "2a");
    }, mensajeEsperado);
});
