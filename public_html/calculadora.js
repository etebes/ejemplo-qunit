var calculadora = (function () {
    function sumar(unNumero, otroNumero) {
        if (isNaN(unNumero) || (isNaN(otroNumero))) {
            throw "El metodo espera 2 numeros enteros";
        }
        return unNumero + otroNumero;
    }
    return{
        sumar: sumar
    };
})();